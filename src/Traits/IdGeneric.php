<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 06/12/2018
 * Time: 15:40
 */

namespace App\Traits;


trait IdGeneric
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}