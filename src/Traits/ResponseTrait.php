<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 10:47
 */

namespace App\Traits;

header('Content-Type: application/json; charset=UTF-8');

trait ResponseTrait
{
    protected $codeSuccess = true;
    protected $codeError = false;
    protected $codeWarning = 2;
    protected $codeInfo = 1;
    protected $messageSuccess = 'Blz, deu certo! ';
    protected $messageError = 'Vish, deu errado! ';
    protected $messageInfo = 'INFORMAÇÃO ';
    protected $messageWarning = 'ADVERTÊNCIA ';
    protected $validate = 'VALIDAÇÃO ';

    public function success ($code, $message, $data)
    {
        return json_encode($this->format($code ,$message, $data));

    }

    public function error ($code, $message, $data)
    {
        return json_encode($this->format($code ,$message, $data));
    }

    public function info ($code, $message, $data)
    {
        return json_encode($this->format($code ,$message, $data));
    }

    public function warning ($code, $message, $data)
    {
        return json_encode($this->format($code ,$message, $data));
    }

    private function format ($code, $message, $data) : array
    {
        return [
            'code'    => $code,
            'message' => $message,
            'data'  => $data
        ];
    }
}