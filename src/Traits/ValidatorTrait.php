<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 02:54
 */

namespace App\Traits;


use App\Exception\ValidationException;
use Doctrine\Common\Annotations\AnnotationRegistry;
use http\Env\Response;
use Symfony\Component\Validator\Validation;



trait ValidatorTrait
{

    /**
     * @var array
     */
    private $errors = [];


    /**
     * @param $class
     * @throws ValidationException
     */
    public function valid($class)
    {
        AnnotationRegistry::registerLoader('class_exists');
        $validateClass = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $errors = $validateClass->validate($class);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $this->errors[] = $error->getPropertyPath() . ": " . $error->getMessage() . ". ";
            }
            $string = implode(" ", $this->errors);
            throw new ValidationException($string, 9999);
        }

    }

}