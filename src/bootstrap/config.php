<?php

use App\BD\Connection;
use App\Controller\GoalController;
use App\Controller\GroupController;
use App\Controller\GroupUserController;
use App\Controller\SeasonController;
use App\Factory\People\PeopleFactory;
use App\Factory\People\PeopleFactoryInterface;
use App\Service\Goal\GoalService;
use App\Service\Goal\GoalServiceInterface;
use App\Service\Group\GroupService;
use App\Service\Group\GroupServiceInterface;
use App\Service\GroupUser\GroupUserService;
use App\Service\GroupUser\GroupUserServiceInterface;
use App\Service\Improvement\ImprovementService;
use App\Service\Improvement\ImprovementServiceInterface;
use App\Service\Login\LoginService;
use App\Service\Login\LoginServiceInterface;
use App\Service\Login\Session\Session;
use App\Service\Login\Session\SessionInterface;
use App\Service\People\PeopleService;
use App\Service\People\PeopleServiceInterface;
use App\Service\Season\SeasonService;
use App\Service\Season\SeasonServiceInterface;
use App\Service\User\UserService;
use App\Service\User\UserServiceInterface;
use function DI\get;
use Doctrine\ORM\EntityManager;


return array(

    EntityManager::class => function () {
        return Connection::connection();
    },

    GroupServiceInterface::class => \Di\autowire(GroupService::class)->constructor(get(EntityManager::class)),
    GroupController::class => \Di\autowire(GroupController::class)->constructor(get(GroupServiceInterface::class)),
    GroupUserController::class => \Di\autowire(GroupUserController::class)->constructor(get(GroupUserServiceInterface::class)),
    PeopleServiceInterface::class => \Di\autowire(PeopleService::class)->constructor(get(EntityManager::class)),
    PeopleFactoryInterface::class => \Di\autowire(PeopleFactory::class)->constructor(get(EntityManager::class)),
    GroupUserServiceInterface::class => \Di\autowire(GroupUserService::class)->constructor(get(EntityManager::class)),
    PeopleFactory::class => \Di\autowire(PeopleFactory::class)->constructor(get(EntityManager::class)),
    UserServiceInterface::class => \Di\autowire(UserService::class)->constructor(
        get(EntityManager::class),
        get(PeopleServiceInterface::class),
        get(GroupUserServiceInterface::class),
        get(SessionInterface::class)
    ),

    Session::class => \Di\autowire(Session::class)->constructor(get(EntityManager::class)),
    SessionInterface::class => \DI\autowire(Session::class),
    LoginService::class => \Di\autowire(LoginService::class)->constructor(
        get(EntityManager::class),
        get(SessionInterface::class),
        get(UserServiceInterface::class)
    ),
    LoginServiceInterface::class => \DI\autowire(LoginService::class),

    GoalController::class => \Di\autowire(GoalController::class)->constructor(get(GoalServiceInterface::class)),
    GoalServiceInterface::class => \Di\autowire(GoalService::class)->constructor(
        get(EntityManager::class),
        get(SessionInterface::class)
    ),

    SeasonServiceInterface::class => \DI\autowire(SeasonService::class)->constructor(
        get(EntityManager::class),
        get(SessionInterface::class)
    ),

    ImprovementServiceInterface::class => \DI\autowire(ImprovementService::class)->constructor(
        get(EntityManager::class),
        get(SessionInterface::class)
    ),

);