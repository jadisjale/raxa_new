<?php

try {
    $builder = new DI\ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->addDefinitions(require_once '../bootstrap/config.php');

    $di = $builder->build();
    return $di;
} catch (Exception $e) {
}
