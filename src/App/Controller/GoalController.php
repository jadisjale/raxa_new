<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 15:58
 */

namespace App\Controller;


use App\Service\Goal\GoalServiceInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;
use Throwable;


class GoalController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var GoalServiceInterface
     */
    private $service;


    /**
     * GoalController constructor.
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(GoalServiceInterface::class);
    }

    public function save(Request $request, Response $response, $args)
    {
        return $this->service->save($request->getParsedBody());
    }

    public function update(Request $request, Response $response, $args)
    {
        try {
            return $this->service->update($request->getAttribute('id'), $request->getParsedBody());
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function delete(Request $request, Response $response, $args)
    {

        try {
            $data = [
                'id' => $request->getParsedBody()['id'],
                'token' => $args['token']
            ];
            return $this->service->delete($data);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function statistics(Request $request, Response $response, $data)
    {
        try {
            return $this->service->statistics($data['token']);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function statisticsToFriend(Request $request, Response $response, $data)
    {
        try {
            return $this->service->statisticsByGroupUser($data['token'], $data['id']);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function average(Request $request, Response $response, $data)
    {
        try {
            return $this->service->average($data['token']);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function getListFrequency(Request $request, Response $response, $token)
    {
        try {
            return $this->service->getListFrequency($token['token']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getListVictory(Request $request, Response $response, $token)
    {
        try {
            return $this->service->getListVictory($token['token']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getListGoals(Request $request, Response $response, $token)
    {
        try {
            return $this->service->getListGoals($token['token']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function saveNewGoal(Request $request, Response $response, $data)
    {
        try {
            if (isset($request->getParsedBody()['goal_id'])) {
                return $this->service->update($request->getParsedBody()['goal_id'], $request->getParsedBody());
            }
            return $this->service->saveNewGoal($data['user'], $request->getParsedBody());
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getHistory(Request $request, Response $response, $data)
    {
        try {
            return $this->service->getHistory($data['token']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getHistoryById(Request $request, Response $response, $data)
    {
        try {
            return $this->service->getHistoryById($data['id']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getListGoalsBySession(Request $request, Response $response, $data)
    {
        try {
            return $this->service->getListGoalsBySession($data['id']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getListVictoryBySession(Request $request, Response $response, $data)
    {
        try {
            return $this->service->getListVictoryBySession($data['id']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    public function getListFrequencyBySession(Request $request, Response $response, $data)
    {
        try {
            return $this->service->getListFrequencyBySession($data['id']);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

}