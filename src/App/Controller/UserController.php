<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:55
 */

namespace App\Controller;


use App\Service\User\UserServiceInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;
use Slim\Http\UploadedFile;
use Throwable;


class UserController
{

    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var UserServiceInterface
     */
    private $service;


    /**
     * Undocumented function
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container) {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(UserServiceInterface::class);

    }

    /**
     * Método de Exemplo
     *
     * @param Request $request
     * @param Response $response
     * @param [type] $request
     * @return void Response
     */
    public function save(Request $request, Response $response, $args)
    {
        try{
            return $this->service->save($request->getParsedBody());
        }catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * Método de Exemplo
     *
     * @param Request $request
     * @param Response $response
     * @param [type] $request
     * @return void Response
     */
    public function photo(Request $request, Response $response, $args)
    {
        try{
            /** @var UploadedFile $arquivo */
            $arquivo = $request->getUploadedFiles()['photo'];
            $data = base64_encode(file_get_contents($arquivo->file));
            $args['photo'] = "data:image/jpeg;base64," . $data;
            return $this->service->photo($args);
        }catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function all()
    {
        try{
            return $this->service->all();
        }catch (Throwable $e) {
            $e->getMessage();
        }
    }

    public function getUsersByGroup(Request $request, Response $response, $data) {
        return $this->service->getUsersByGroup($data['token']);
    }

    public function saveUserAndNewGroup(Request $request, Response $response, $data)
    {
        return $this->service->saveUserAndNewGroup($request->getParsedBody());
    }

    public function profile(Request $request, Response $response, $data)
    {
        return $this->service->profile($request->getParsedBody());
    }

    public function updateDataUser(Request $request, Response $response, $data)
    {
        return $this->service->updateDataUser($data['token'], $request->getParsedBody());
    }

    public function getById(Request $request, Response $response, $data)
    {
        return $this->service->getById($data['token']);
    }

    public function tokenFcm(Request $request, Response $response)
    {
        return $this->service->tokenFcm($request->getParsedBody());
    }
}