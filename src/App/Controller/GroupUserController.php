<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/01/2019
 * Time: 13:33
 */

namespace App\Controller;


use App\Service\GroupUser\GroupUserServiceInterface;
use Slim\Container;
use Throwable;


class GroupUserController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var GroupUserServiceInterface
     */
    private $service;


    /**
     * GroupUserController constructor.
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(GroupUserServiceInterface::class);

    }


    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}
