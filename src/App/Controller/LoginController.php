<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 21:43
 */

namespace App\Controller;

use App\Service\Login\LoginServiceInterface;
use Slim\Container;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Throwable;


class LoginController
{

    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var LoginServiceInterface
     */
    private $service;

    /**
     * LoginController constructor.
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(LoginServiceInterface::class);
    }

    public function auth(Request $request, Response $response, $args) {
        $data = $request->getParsedBody();
        return $this->service->auth(
            (int) $data['id_group'],
            $data['login'],
            $data['password']
        );
    }

    public function logout(Request $request, Response $response, $args)
    {
        return $this->service->logout($args['token']);
    }

}