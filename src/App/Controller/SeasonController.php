<?php

/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 14:51
 */

namespace App\Controller;

use App\Service\Season\SeasonServiceInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;

class SeasonController
{
    private $container;

    /** @var SeasonServiceInterface */
    private $service;

    /**
     * SeasonController constructor.
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        /** @var \DI\Container $di */
        $di = $this->container->get('di');
        $this->service = $di->get(SeasonServiceInterface::class);
    }

    public function getAllSeasonByGroup(Request $request, Response $response, $data)
    {
        return $this->service->all($data['token']);
    }

    public function save(Request $request, Response $response, $data)
    {
        return $this->service->save($request->getParsedBody());
    }
}