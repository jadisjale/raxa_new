<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:55
 */

namespace App\Controller;

use App\Service\Group\GroupServiceInterface;
use App\Service\Improvement\ImprovementServiceInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;
use Throwable;

class GroupController
{

    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var GroupServiceInterface
     */
    private $service;


    /**
     * Undocumented function
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(GroupServiceInterface::class);

    }

    /**
     * Método de Exemplo
     *
     * @param Request $request
     * @param Response $response
     * @param [type] $request
     * @return void Response
     */
    public function save(Request $request, Response $response, $args)
    {
        try {
            return $this->service->save($request->getParsedBody());
        } catch (Throwable $e) {
            throw $e;
        }
    }

    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            throw $e;
        }
    }

}