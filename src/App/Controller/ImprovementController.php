<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:55
 */

namespace App\Controller;


use App\Service\Improvement\ImprovementService;
use App\Service\Improvement\ImprovementServiceInterface;
use App\Service\User\UserServiceInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Container;
use Slim\Http\UploadedFile;
use Throwable;


class ImprovementController
{

    /**
     * Container Class
     * @var [object]
     */
    private $container;

    /**
     * @var ImprovementServiceInterface
     */
    private $service;


    /**
     * Undocumented function
     * @param Container $container
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     * @throws \Interop\Container\Exception\ContainerException
     */
    public function __construct(Container $container) {
        $this->container = $container;

        /** @var \DI\Container $di */
        $di = $this->container->get('di');

        $this->service = $di->get(ImprovementService::class);

    }

    /**
     * Método de Exemplo
     *
     * @param Request $request
     * @param Response $response
     * @param [type] $request
     * @return void Response
     */
    public function save(Request $request, Response $response, $args)
    {
        try{
            return $this->service->save($request->getParsedBody());
        }catch (Throwable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Método de Exemplo
     *
     * @param Request $request
     * @param Response $response
     * @param [type] $request
     * @return void Response
     */
    public function all(Request $request, Response $response, $args)
    {
        try{
            /** @var UploadedFile $arquivo */
            $arquivo = $request->getUploadedFiles()['photo'];
            $data = base64_encode(file_get_contents($arquivo->file));
            $args['photo'] = "data:image/jpeg;base64," . $data;
            return $this->service->photo($args);
        }catch (Throwable $e) {
            $e->getMessage();
        }
    }
}