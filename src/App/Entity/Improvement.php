<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:23
 */

namespace App\Entity;


use App\Traits\IdGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Improvement\ImprovementRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.improvement")
 */
class Improvement
{
    use IdGeneric;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Usuário obrigatório")
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Login obrigatório")
     *
     */
    private $description;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

}