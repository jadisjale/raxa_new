<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:23
 */

namespace App\Entity;


use App\Traits\IdGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\Group\GroupRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.groups")
 */
class Group
{
    use IdGeneric;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     * @Assert\NotBlank(message="Nome obrigatório")
     *
     */
    private $name;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\GroupUser", cascade={"persist"}, mappedBy="group")
     */
    private $user;

    public function __construct()
    {
        $this->user = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
}