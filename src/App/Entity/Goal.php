<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 14:32
 */

namespace App\Entity;


use App\Traits\IdGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Goal\GoalRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.goals")
 */
class Goal
{
    use IdGeneric;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $victory;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $draw;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $defeat;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $goals;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Assert\Type(
     *     type="datetime",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo Data obrigatório")
     *
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $assistance;

    /**
     * @var GroupUser
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\GroupUser", cascade={"persist"}, inversedBy="goal")
     * @ORM\JoinColumn(name="group_user_id", referencedColumnName="id", nullable=false)
     */
    private $groupUser;

    /**
     * @var \App\Entity\Season
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Season")
     * @ORM\JoinColumn(name="season", referencedColumnName="id")
     * @Assert\NotBlank(message="Campo obrigatório")
     */
    private $season;

    /**
     * @return Season
     */
    public function getSeason(): Season
    {
        return $this->season;
    }

    /**
     * @param Season $season
     * @return Goal
     */
    public function setSeason(Season $season): Goal
    {
        $this->season = $season;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVictory()
    {
        return $this->victory;
    }

    /**
     * @param mixed $victory
     * @return Goal
     */
    public function setVictory($victory)
    {
        $this->victory = $victory;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDraw()
    {
        return $this->draw;
    }

    /**
     * @param mixed $draw
     * @return Goal
     */
    public function setDraw($draw)
    {
        $this->draw = $draw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefeat()
    {
        return $this->defeat;
    }

    /**
     * @param mixed $defeat
     * @return Goal
     */
    public function setDefeat($defeat)
    {
        $this->defeat = $defeat;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * @param mixed $goals
     * @return Goal
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     * @return Goal
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAssistance()
    {
        return $this->assistance;
    }

    /**
     * @param mixed $assistance
     * @return Goal
     */
    public function setAssistance($assistance)
    {
        $this->assistance = $assistance;
        return $this;
    }

    /**
     * @return GroupUser
     */
    public function getGroupUser(): GroupUser
    {
        return $this->groupUser;
    }

    /**
     * @param GroupUser $groupUser
     * @return Goal
     */
    public function setGroupUser(GroupUser $groupUser): Goal
    {
        $this->groupUser = $groupUser;
        return $this;
    }

}