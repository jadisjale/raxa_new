<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 08/01/2019
 * Time: 19:39
 */

namespace App\Entity;


use App\Traits\IdGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\GroupUser\GroupUserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.groups_users")
 */
class GroupUser
{
    use IdGeneric;


    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist"}, inversedBy="groups")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Group", cascade={"persist"}, inversedBy="user")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", nullable=false)
     */
    private $group;

    /**
     * @var Goal
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Goal", cascade={"persist"}, mappedBy="groupUser")
     */
    private $goal;


    public function __construct()
    {
        $this->goal = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return GroupUser
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroup():Group
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     * @return GroupUser
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @param Goal addGoal
     * @return GroupUser
     */
    public function addGoal(Goal $goal)
    {
        //$this->users[] = $user;
        $this->goal->add($goal);
        $goal->setGroupUser($this);
        return $this;
    }

    /**
     * @return Goal
     */
    public function getGoal()
    {
        return $this->goal;
    }
}