<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 01/01/19
 * Time: 15:23
 */

namespace App\Entity;


use App\Traits\IdGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.users")
 */
class User
{
    use IdGeneric;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\People")
     * @ORM\JoinColumn(name="people_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Usuário obrigatório")
     */
    private $people;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Login obrigatório")
     *
     */
    private $login;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     * @Assert\NotBlank(message="Senha obrigatório")
     *
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     *
     */
    private $token;

    /**
     * @ORM\Column(type="string")
     *
     */
    private $token_fcm;

    /**
     * @return mixed
     */
    public function getTokenFcm()
    {
        return $this->token_fcm;
    }

    /**
     * @param mixed $token_fcm
     */
    public function setTokenFcm(string $token_fcm): void
    {
        $this->token_fcm = $token_fcm;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @ORM\Column(name="is_admin", type="boolean")
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     * @Assert\NotBlank(message="informe se o usuário é um admin")
     *
     */
    private $isAdmin;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     */
    private $photo;

    /**
     *
     * @ORM\OneToMany(targetEntity="App\Entity\GroupUser", cascade={"persist"}, mappedBy="user")
     */
    private $group;

    public function __construct()
    {
        $this->group = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getPeople(): People
    {
        return $this->people;
    }

    /**
     * @param mixed $people
     * @return User
     */
    public function setPeople(People $people): User
    {
        $this->people = $people;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param mixed $isAdmin
     * @return User
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

}