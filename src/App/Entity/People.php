<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 06/01/19
 * Time: 11:19
 */

namespace App\Entity;

use App\Traits\IdGeneric;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\People\PeopleRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="raxa.peoples")
 */
class People
{
   use IdGeneric;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Nome obrigatório")
     *
     */
    private $name;


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return People
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }
}