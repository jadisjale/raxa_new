<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 15:22
 */

namespace App\Entity;

use App\Traits\IdGeneric;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\Season\SeasonRepository")
 * @ORM\Table(name="raxa.season")
 */
class Season
{

    use IdGeneric;


    /**
     * @ORM\Column(type="string", name="descrpition")
     *
     * @Assert\Type(
     *     type="string",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo obrigatório")
     *
     */
    private $descrpition;

    /**
     * @return mixed
     */
    public function getDescrpition()
    {
        return $this->descrpition;
    }

    /**
     * @param mixed $descrpition
     * @return Season
     */
    public function setDescrpition($descrpition)
    {
        $this->descrpition = $descrpition;
        return $this;
    }

    /**
     * @ORM\Column(type="datetime", name="start_date")
     *
     * @Assert\Type(
     *     type="datetime",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     *
     * @Assert\NotBlank(message="Campo Data obrigatório")
     *
     */
    private $start_date;

    /**
     * @ORM\Column(type="datetime", name="end_date")
     *
     */
    private $end_date;

    /**
     * @var Group
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Group", mappedBy="id")
     * @ORM\JoinColumn(name="groups", referencedColumnName="id", nullable=true)
     */
    private $groups;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     *
     * @Assert\Type(
     *     type="boolean",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     * @Assert\NotBlank(message="informe se o usuário é um admin")
     *
     */
    private $isActive;

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate(\DateTime $start_date): void
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate(\DateTime $end_date): void
    {
        $this->end_date = $end_date;
    }

    /**
     * @return Group
     */
    public function getGroups(): Group
    {
        return $this->groups;
    }

    /**
     * @param Group $groups
     */
    public function setGroups(Group $groups): void
    {
        $this->groups = $groups;
    }

    /**
     * @return mixed
     */
    public function getisActive() :  bool
    {
        return $this->isActive;
    }

    /**
     * @param mixed $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

}