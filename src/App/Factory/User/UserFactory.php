<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 14:00
 */

namespace App\Factory\User;


use App\Entity\User;
use App\Factory\FactoryInterface;
use Throwable;


class UserFactory implements FactoryInterface
{

    static public function make(array $data)
    {
        try {
            $user = new User();
            return $user
                ->setPeople($data['people'])
                ->setLogin($data['login'])
                ->setIsAdmin((bool) $data['is_admin'])
                ->setPassword(md5($data['password']))
                ->setPhoto($data['photo'])
            ;
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}