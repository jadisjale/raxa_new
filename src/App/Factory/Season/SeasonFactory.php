<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 15:21
 */

namespace App\Factory\Season;

use App\Entity\Season;
use App\Factory\FactoryInterface;

class SeasonFactory implements FactoryInterface
{

    static public function make(array $data)
    {
        $season = new Season();
        $season->setDescrpition($data['descrpition']);
        $season->setStartDate(new \DateTime());
        $season->setIsActive(true);
        $season->setGroups($data['group']);
        return $season;
    }
}