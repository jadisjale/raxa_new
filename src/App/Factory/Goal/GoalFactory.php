<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 15:40
 */

namespace App\Factory\Goal;


use App\Entity\Goal;
use App\Factory\FactoryInterface;
use function DI\string;
use Throwable;


class GoalFactory implements FactoryInterface
{

    static public function make(array $data)
    {
        try{
            $goal = new Goal();
            return $goal
                ->setVictory((integer) $data['victory'])
                ->setDraw((integer) $data['draw'])
                ->setDefeat((integer) $data['defeat'])
                ->setGoals((integer) $data['goals'])
                ->setDate(new \DateTime($data['date']))
                ->setAssistance((integer) $data['assistance'])
                ->setGroupUser($data['groupUser'])
                ->setSeason($data['season'])
            ;
        }catch (Throwable $e){
            var_dump($e->getMessage());die;
            $e->getMessage();
        }
    }
}