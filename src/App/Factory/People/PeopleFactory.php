<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 06/01/19
 * Time: 11:40
 */

namespace App\Factory\People;

use App\Entity\People;
use App\Entity\User;
use App\Factory\FactoryInterface;
use Exception;

class PeopleFactory implements FactoryInterface
{
    static public function make(array $data)
    {
        try {
            $p = new People();
            return $p->setName($data['name']);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

}