<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 13:34
 */

namespace App\Factory;


interface FactoryInterface
{
    static public function make(array $data);
}