<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 13:34
 */

namespace App\Factory\GroupUser;



use App\Entity\GroupUser;
use App\Factory\FactoryInterface;
use Exception;


class GroupUserFactory implements FactoryInterface
{
    static public function make(array $data)
    {
        try {
            $g = new GroupUser();
            return $g
                ->setUser($data['user'])
                ->setGroup($data['group'])
            ;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }
}