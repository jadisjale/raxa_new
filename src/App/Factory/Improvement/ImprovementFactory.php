<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 26/11/2019
 * Time: 22:12
 */

namespace App\Factory\Improvement;


use App\Entity\Improvement;

class ImprovementFactory
{
    /**
     * @param array $data
     * @return Improvement
     * @throws \Exception
     */
    static public function make(array $data) : Improvement
    {
        try {
            $improvement = new Improvement();
            $improvement->setDescription($data['description']);
            $improvement->setUser($data['user']);
            return $improvement;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}