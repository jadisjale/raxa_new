<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 13:34
 */

namespace App\Factory\Group;


use App\Entity\Group;
use App\Factory\FactoryInterface;
use Exception;


class GroupFactory implements FactoryInterface
{
    static public function make(array $data)
    {
        try {
            $g = new Group();
            return $g->setName($data['name']);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    static public function makeWithNewUser(array $data)
    {
        try {
            $g = new Group();
            return $g->setName($data['group']);
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

}