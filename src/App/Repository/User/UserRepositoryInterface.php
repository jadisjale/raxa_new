<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 10:49
 */

namespace App\Repository\User;


interface UserRepositoryInterface
{

#todo: -  LISTA TODOS OS USUÁRIOS
    public function all();

    public function getUsersByGroup ($id_group, $id_user);

    public function logout (int $id_user);
}