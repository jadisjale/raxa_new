<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 10:49
 */

namespace App\Repository\User;


use App\Entity\User;
use App\Repository\AbstractRepository;
use App\Service\Login\Session\Session;
use App\Traits\ResponseTrait;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use Throwable;
use Doctrine\ORM\Query\Expr\Join;


class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    use ResponseTrait;

    /**
     * @return mixed
     * @throws Exception
     */
    public function all()
    {
        try {
            return $this->createQueryBuilder('u')
                        ->select('u.id, p.name, u.login, u.isAdmin, u.photo')
                        ->innerJoin('u.people', 'p')
                        ->innerJoin('u.group','g')
                        ->where('g.group = :id')
                        ->setParameter('id', Session::getGroup())
                        ->getQuery()
                        ->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);
        } catch (Throwable $e) {
           throw new Exception($e->getMessage());
        }
    }

    public function getUsersByGroup($id_group, $id_user)
    {
        try {
            return $this->createQueryBuilder('user')
                ->select('user.id, user.photo, user.login, people.name, groupUser.id as group_user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'user = groupUser.user')
                ->where('groupUser.group = :id')
                ->andWhere('groupUser.user <> :id_user')
                ->setParameter(':id', $id_group)
                ->setParameter(':id_user', $id_user)
                ->orderBy('people.name', 'asc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function logout(int $id_user)
    {
        try {

            /** @var EntityManager $em */
            $em = $this->getEntityManager();
            /** @var User $user */
            $user = $em->getRepository(User::class)->find($id_user);
            $user->setToken(null);
            $this->getEntityManager()->persist($user);
            $em->persist($user);
            $em->flush();


        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function photo(int $id_user, string $base64)
    {
        try {
            /** @var EntityManager $em */
            $em = $this->getEntityManager();
            /** @var User $user */
            $user = $em->getRepository(User::class)->find($id_user);
            $user->setPhoto($base64);
            $this->getEntityManager()->persist($user);
            $em->persist($user);
            $em->flush();

        } catch (Throwable $e) {
            throw new Exception($e->getMessage());
        }
    }
}