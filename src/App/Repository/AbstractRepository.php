<?php

namespace App\Repository;


use App\Service\Login\Session\SessionInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\UnitOfWork;


abstract class AbstractRepository extends EntityRepository
{

    public function getReference($id, $class = null)
    {
        try {
            if (!$class) {
                $class = $this->getClassName();
            }
            return $this->getEntityManager()->getReference($class, $id);
        } catch (ORMException $e) {
            $e->getMessage();
        }
    }

    public function save($entity)
    {
        try {
            if ($this->getEntityManager()->getUnitOfWork()->getEntityState($entity) == UnitOfWork::STATE_NEW) {
                $this->getEntityManager()->persist($entity);
            }
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            $e->getMessage();
        }
    }

    public function delete($id)
    {
        try {
            $entity = $this->getReference($id);
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            $e->getMessage();
        }
    }
}