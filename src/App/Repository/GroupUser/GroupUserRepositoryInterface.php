<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/01/2019
 * Time: 14:22
 */

namespace App\Repository\GroupUser;


interface GroupUserRepositoryInterface
{
    public function all();
}