<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 10:05
 */

namespace App\Repository\GroupUser;


use App\Repository\AbstractRepository;
use Throwable;


class GroupUserRepository extends AbstractRepository implements GroupUserRepositoryInterface
{

    public function all()
    {
        try {
            return $this->createQueryBuilder('g_u')
                ->select('u.id, p.name, u.login, u.isAdmin, u.photo')
                ->innerJoin('g_u.user','u')
                ->innerJoin('u.people', 'p')
                ->where('u.id = g_u.user.user_id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}