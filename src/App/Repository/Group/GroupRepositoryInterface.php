<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 08:48
 */

namespace App\Repository\Group;


interface GroupRepositoryInterface
{
    // LISTA TODOS OS GRUPOS
    public function all();
}