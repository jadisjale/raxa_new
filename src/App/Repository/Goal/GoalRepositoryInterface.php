<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/01/2019
 * Time: 15:07
 */

namespace App\Repository\Goal;


interface GoalRepositoryInterface
{
    public function statistics($id_group_user);

    public function getListVictory($id_group_user);
    public function getListFrequencyByIdGroupUser($id_group_user);
    public function getListDraw();
    public function getListDefeat();
    public function getListGoals($id_group);
    public function getListAssistance();
    public function getListFrequency($id_group);

    public function getListVictoryBySeason($id_season);
    public function getListGoalsBySeason($id_season);
    public function getListFrequencyBySeason($id_season);

    public function getHistory($id_group_user);
}