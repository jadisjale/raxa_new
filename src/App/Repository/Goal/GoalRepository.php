<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 15:50
 */

namespace App\Repository\Goal;

use App\Repository\AbstractRepository;
use Doctrine\ORM\Query\Expr\Join;
use Throwable;


class GoalRepository extends AbstractRepository implements GoalRepositoryInterface
{

    public function statistics($id_group_user)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('user.id, user.photo, people.name, sum(goal.goals) AS goals, sum(goal.victory) AS victory, sum(goal.draw) AS draw, sum(goal.defeat) AS defeat, sum(goal.assistance) AS assistance')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('groupUser.id = :id')
                ->setParameter(':id', $id_group_user)
                ->andWhere('season.end_date is null')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListVictory($id_group)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('user.id, people.name, user.login, user.photo, sum(goal.victory) AS victory')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('groupUser.group = :id')
                ->setParameter(':id', $id_group)
                ->andWhere('season.end_date is null')
                ->orderBy('victory', 'desc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListDraw()
    {
        // TODO: Implement getListDraw() method.
    }

    public function getListDefeat()
    {
        // TODO: Implement getListDefeat() method.
    }

    public function getListGoals($id_group)
    {
        return $this->createQueryBuilder('goal')
            ->select('user.id, people.name, user.login, user.photo, sum(goal.goals) AS goals')
            ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
            ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
            ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
            ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
            ->groupBy('user.id, people.name')
            ->where('groupUser.group = :id')
            ->setParameter(':id', $id_group)
            ->andWhere('season.end_date is null')
            ->orderBy('goals', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getListAssistance()
    {
        // TODO: Implement getListAssistance() method.
    }


    public function getListFrequency($id_group)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('user.id, people.name, user.login, user.photo, count(goal.id) AS frequency')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('groupUser.group = :id')
                ->setParameter(':id', $id_group)
                ->andWhere('season.end_date is null')
                ->orderBy('frequency', 'desc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListFrequencyByIdGroupUser($id_group_user)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('count(goal.id) AS frequency')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('groupUser = :id')
                ->setParameter(':id', $id_group_user)
                ->andWhere('season.end_date is null')
                ->orderBy('frequency', 'desc')
                ->getQuery()
                ->getSingleResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getHistory($id_group_user)
    {
        return $this->createQueryBuilder('goal')
            ->select('user.id, people.name, goal.id as goal_id, goal.victory, goal.draw, goal.defeat, goal.goals, goal.date, goal.assistance')
            ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
            ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
            ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
            ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
            ->where('groupUser.id = :id')
            ->setParameter(':id', $id_group_user)
            ->andWhere('season.end_date is null')
            ->orderBy('goal.date', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getListVictoryBySeason($id_season)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('user.id, people.name, user.login, user.photo, sum(goal.victory) AS victory')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('season.id = :id')
                ->setParameter(':id', $id_season)
                ->orderBy('victory', 'desc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getListGoalsBySeason($id_season)
    {
        return $this->createQueryBuilder('goal')
            ->select('user.id, people.name, user.login, user.photo, sum(goal.goals) AS goals')
            ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
            ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
            ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
            ->join('App\Entity\Season', 'season', Join::WITH, 'season.id = goal.season')
            ->groupBy('user.id, people.name')
            ->where('season.id = :id')
            ->setParameter(':id', $id_season)
            ->orderBy('goals', 'desc')
            ->getQuery()
            ->getResult();
    }

    public function getListFrequencyBySeason($id_season)
    {
        try {
            return $this->createQueryBuilder('goal')
                ->select('user.id, people.name, user.login, user.photo, count(goal.id) AS frequency')
                ->join('App\Entity\GroupUser', 'groupUser', Join::WITH, 'goal.groupUser = groupUser')
                ->join('App\Entity\User', 'user', Join::WITH, 'user = groupUser.user')
                ->join('App\Entity\People', 'people', Join::WITH, 'people = user.people')
                ->join('App\Entity\Season', 'season', Join::WITH, 'season = goal.season')
                ->groupBy('user.id, people.name')
                ->where('season.id = :id')
                ->setParameter(':id', $id_season)
                ->orderBy('frequency', 'desc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new \Exception($e->getMessage());
        }
    }
}