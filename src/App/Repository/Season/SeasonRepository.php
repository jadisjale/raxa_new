<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 07/07/2019
 * Time: 00:00
 */

namespace App\Repository\Season;


use App\Entity\Group;
use App\Repository\AbstractRepository;

class SeasonRepository extends AbstractRepository implements SeasonRepositoryInterface
{

    public function getByGroup(Group $group)
    {
        try {
            return $this->createQueryBuilder('season')
                ->select('season')
                ->where('season.groups = :idGroup')
                ->setParameter('idGroup', $group->getId())
                ->getQuery()
                ->getOneOrNullResult();
        } catch (\Throwable $e) {
            throw new $e;
        }
    }

    public function getLastSeasonActive(Group $group)
    {
        try {
            return $this->createQueryBuilder('season')
                ->select('season')
                ->where('season.groups = :idGroup')
                ->setParameter('idGroup', $group->getId())
                ->andWhere('season.isActive = true')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (\Throwable $e) {
            throw new $e;
        }
    }

    public function getAllByGroupId(Group $group)
    {
        try {
            return $this->createQueryBuilder('season')
                ->select('season')
                ->where('season.groups = :idGroup')
                ->setParameter('idGroup', $group->getId())
                ->orderBy('season.id', 'desc')
                ->getQuery()
                ->getResult();
        } catch (\Exception $e) {
            throw new \Exception('Não há temporada cadastrada para o grupo: ' . $group->getName());
        }
    }
}