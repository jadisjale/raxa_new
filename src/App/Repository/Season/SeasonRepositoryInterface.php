<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 23:58
 */

namespace App\Repository\Season;


use App\Entity\Group;

interface SeasonRepositoryInterface
{
    public function getByGroup(Group $idGroup);
    public function getLastSeasonActive(Group $idGroup);
    public function getAllByGroupId(Group $idGroup);
}