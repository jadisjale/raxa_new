<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 23:03
 */

namespace App\Middleware;

use App\Traits\ResponseTrait;
use Exception;
use Psr\Http\Message\ResponseInterface;
use SlimSession\Helper;

class AuthMiddleware
{

    use ResponseTrait;

    /**
     * @param $request
     * @param $response
     * @param $next
     * @return mixed
     * @throws Exception
     */
    public function __invoke($request, ResponseInterface $response, $next)
    {
            return $next($request, $response);

        if (isset($_SESSION['data']['group_user'])) {
        }

        $data = [
            'code' => 401,
            'message' => 'Usuário não autenticado'
        ];

        $response
            ->withHeader('Content-type', 'application/json')
            ->getBody()
            ->write(json_encode($data))
        ;

        return $response;

    }

}