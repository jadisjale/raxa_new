<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 08/11/2018
 * Time: 20:25
 */

namespace App\BD;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;


class DataBase
{
    private static $paths = [
        __DIR__ . '/../Entity/'
    ];

    private static $isDevMode = true;


    // the connection configuration
    private static $dbParams = array(
        'driver'   => 'pdo_pgsql',
        'host'     => 'localhost',
        'port'     => '5432',
        'user'     => 'postgres',
        'password' => '2710',
        'dbname'   => 'postgres'
    );

    public static function getEntityManager()
    {
        try {
            $cache = new ArrayCache();
            $config = new Configuration();
            $config->setMetadataCacheImpl($cache);
            $driverImpl = $config->newDefaultAnnotationDriver(static::$paths);
            $config->setMetadataDriverImpl($driverImpl);
            $config->setQueryCacheImpl($cache);
            //$config->setProxyDir(__DIR__ . '/../proxies');
            //$config->setProxyNamespace('app\proxies');
            $config->setAutoGenerateProxyClasses(false);
            $config = Setup::createAnnotationMetadataConfiguration(static::$paths, static::$isDevMode, __DIR__ . '/../proxies', $cache, false);
            return EntityManager::create(static::$dbParams, $config);
        } catch (ORMException $e) {
            $e->getMessage();
        }
    }
}