<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 04/01/19
 * Time: 22:27
 */

namespace App\BD;


use Doctrine\ORM\EntityManager;

interface ConnectionInterface
{
    static public function connection() : EntityManager;
}