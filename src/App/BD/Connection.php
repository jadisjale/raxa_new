<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 04/01/19
 * Time: 22:27
 */

namespace App\BD;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationRegistry;


class Connection implements ConnectionInterface
{

    static public function connection(): EntityManager
    {

        $paths = array(__DIR__.'/../../../src/App/Entity');
        $isDevMode = true;

        $connectionParams = [
            'driver'   => 'pdo_pgsql',
            'host'     => 'localhost',
            'port'     => '5432',
            'user'     => 'postgres',
            'password' => '2710',
            'dbname'   => 'raxa'
        ];

        try {
            $cache = new ArrayCache();
            $config = new Configuration();
            $config->setMetadataCacheImpl($cache);
            $config = Setup::createConfiguration($isDevMode);
            //$driverImpl = new AnnotationDriver(new AnnotationReader(), $paths);
            AnnotationRegistry::registerLoader('class_exists');
            $driverImpl = $config->newDefaultAnnotationDriver($paths);
            $config->setMetadataDriverImpl($driverImpl);
            $config->setQueryCacheImpl($cache);
            $config->setProxyDir(__DIR__ . '/../../../src/App/Proxy');
            $config->setProxyNamespace('/../../../src/App/Proxy');
            $config->setAutoGenerateProxyClasses(false);
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, __DIR__ . '/../../../src/App/Proxy', $cache, false);
            return EntityManager::create($connectionParams, $config);
        } catch (ORMException $e) {
            $e->getMessage();
        }

//        $config = Setup::createConfiguration($isDevMode);
//        $driver = new AnnotationDriver(new AnnotationReader(), $paths);
//
//        AnnotationRegistry::registerLoader('class_exists');
//        $config->setMetadataDriverImpl($driver);
//
//        return EntityManager::create($connectionParams, $config);

    }

}