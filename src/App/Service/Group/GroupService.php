<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 04/01/19
 * Time: 22:41
 */

namespace App\Service\Group;

use App\Entity\Group;
use App\Exception\ValidationException;
use App\Factory\Group\GroupFactory;
use App\Service\GroupUser\GroupUserServiceInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use DI\Annotation\Inject;
use Doctrine\ORM\EntityManager;
use Slim\Http\Response;
use Throwable;


class GroupService implements GroupUserServiceInterface
{
    use ValidatorTrait;
    use ResponseTrait;

    /** @var \App\Repository\Group\GroupRepository $groupRepository */
    private $groupRepository;

    /**
     * @Inject()
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * GroupService constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->groupRepository = $this->em->getRepository(Group::class);
    }


    public function save(array $data)
    {
        try {
            $group = GroupFactory::make($data);
            $this->valid($group);
            $this->groupRepository->save($group);
            return $this->success($this->codeSuccess, $this->messageSuccess, $group->getName());
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function all()
    {
       try {
           return $this->success($this->codeSuccess, $this->messageSuccess, $this->groupRepository->all());
       } catch(Throwable $e) {
           $e->getMessage();
       }
    }
}