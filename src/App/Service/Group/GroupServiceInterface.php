<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 04/01/19
 * Time: 22:40
 */

namespace App\Service\Group;

interface GroupServiceInterface
{
    // CRIA UM NOVO GRUPO
    public function save(array $data);

    // LISTA TODOS OS GRUPOS
    public function all();
}