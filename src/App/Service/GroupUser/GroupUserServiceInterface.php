<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 11:07
 */

namespace App\Service\GroupUser;


interface GroupUserServiceInterface
{
    public function save(array $data);

    public function all();
}