<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 09/01/2019
 * Time: 11:06
 */

namespace App\Service\GroupUser;


use App\Entity\GroupUser;
use App\Exception\ValidationException;
use App\Factory\GroupUser\GroupUserFactory;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;
use Exception;
use Throwable;


class GroupUserService implements GroupUserServiceInterface
{
    use ValidatorTrait;
    use ResponseTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \App\Repository\GroupUser\GroupUserRepository
     */
    private $groupUserRepository;

    /**
     * UserService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->groupUserRepository = $this->em->getRepository(GroupUser::class);
    }

    public function save(array $data)
    {
        try {
            $groupUser = GroupUserFactory::make($data);
            $this->valid($groupUser);
            $this->groupUserRepository->save($groupUser);
        } catch (ValidationException $e) {
            $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Exception $e) {
            $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function all()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->groupUserRepository->all());
        } catch (Throwable $e) {
            $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}