<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 06/01/19
 * Time: 11:37
 */

namespace App\Service\People;


use App\Entity\People;
use App\Exception\ValidationException;
use App\Factory\People\PeopleFactory;
use App\Factory\People\PeopleFactoryInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;

class PeopleService implements PeopleServiceInterface
{
    use ValidatorTrait;
    use ResponseTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \App\Repository\People\PeopleRepository $peopleRepository
     */
    private $peopleRepository;

    /**
     * UserService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->peopleRepository = $this->em->getRepository(People::class);
    }

    //ALTER TABLE raxa.people ADD id serial NOT NULL;


    public function save(array $data)
    {
        try {
            $people = PeopleFactory::make($data);
            $this->valid($people);
            $this->peopleRepository->save($people);
            return $people;
        } catch (ValidationException $e) {
            $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (\Throwable $e) {
            $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}