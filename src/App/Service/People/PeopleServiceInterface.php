<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 06/01/19
 * Time: 11:37
 */

namespace App\Service\People;


interface PeopleServiceInterface
{
    public function save(array $data);
}