<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 19:10
 */

namespace App\Service\Login;


interface LoginServiceInterface
{

    public function auth(int $id_group, string $login, string $password);

    public function logout($token);

}