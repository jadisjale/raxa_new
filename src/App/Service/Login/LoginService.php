<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 19:11
 */

namespace App\Service\Login;

use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\User;
use App\Service\Login\Session\SessionInterface;
use App\Service\User\UserServiceInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use function DI\string;
use Doctrine\ORM\EntityManager;

class LoginService implements LoginServiceInterface
{

    use ValidatorTrait;
    use ResponseTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /** @var UserServiceInterface $userService */
    private $userService;


    /**
     * LoginService constructor.
     * @param EntityManager $em
     * @param SessionInterface $session
     * @param UserServiceInterface $userService
     */
    public function __construct(
        EntityManager $em,
        SessionInterface $session,
        UserServiceInterface $userService
    )
    {
        $this->em = $em;
        $this->session = $session;
        $this->userService = $userService;
    }

    public function auth(int $id_group, string $login, string $password)
    {
        try {

            $login_param = new Login();
            $login_param
                ->setLogin($login)
                ->setIdGroup($id_group)
                ->setPassword((string) $password)
            ;

            $this->valid($login_param);

            /** @var User $user */
            $user = $this->em->getRepository(User::class)->findOneBy([
                'login' => $login,
                'password' => md5($password)
            ]);

            if (!$user) {
                return $this->info($this->codeWarning,'Usuário ou Senha errado', $login);
            }

            /** @var Group $group */
            $group = $this->em->getRepository(Group::class)->find($id_group);

            if (!$group) {
                return $this->info($this->codeWarning,'Grupo não encontrado', null);
            }

            /** @var GroupUser $group_user */
            $group_user = $this->em->getRepository(GroupUser::class)->findOneBy([
                'user'  => $user,
                'group' => $group
            ]);

            if ($group_user) {

                $token = $this->makeToken($group_user);

                $user->setToken($token);

                $this->em->persist($user);
                $this->em->flush();

                return $this->success($this->codeSuccess,'Pode logar', [
                    'token' => $token,
                    'isAdmin' => $user->getIsAdmin()
                ]);
            }

            return $this->warning($this->codeWarning,'Esse usuário não pertence a esse grupo', $group->getName());

        } catch (\Exception $e) {
            return $this->error($this->codeWarning, $e->getMessage(), null);
        } catch (\Throwable $e) {
            return $this->error($this->codeError, 'Ocorreu um erro ao logar', null);
        }
    }

    private function makeToken(GroupUser $groupUser) : string
    {
        return $groupUser->getId() . '-' . $this->randomString(15);
    }

    private function randomString($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function logout($token)
    {
        try {
            $this->session->setToken($token);
            $id_user = $this->session->getGroupUser()->getUser()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->userService->logout($id_user));
        } catch (\Throwable $e) {
            return $this->error($this->codeError, 'Não foi possível deslogar', null);
        }
    }
}