<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 22:24
 */

namespace App\Service\Login\Session;


use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\User;

interface SessionInterface
{

    public function getDataSession() : array;

    public function getUserLogged() : User;
    public function getGroupToUser() : Group;
    public function getGroupUser() : GroupUser;

    public function removeDataSession();

    public function setToken($token);
    public function checkToken() : bool;

}