<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 12/01/19
 * Time: 22:25
 */

namespace App\Service\Login\Session;


use App\Entity\Group;
use App\Entity\GroupUser;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class Session implements SessionInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var GroupUser $groupUser
     */
    private $groupUser;

    protected $token;


    /**
     * Session constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getDataSession(): array
    {
       return $_SESSION['data'];
    }

    public function getUserLogged() : User
    {
        return $this->groupUser->getUser();
    }

    public function getGroupUser() : GroupUser
    {
        return $this->groupUser;
    }

    public function getGroupToUser() : Group
    {
        return $this->groupUser->getGroup();
    }

    public function removeDataSession()
    {
        $_SESSION['data'] = '';
    }

    public function setToken($token)
    {
        $this->token = $token;
        if (isset($this->token)) {
            $data = explode('-', $this->token);
            $this->groupUser = $this->em->getRepository(GroupUser::class)->find($data[0]);
        }
        return $this;
    }

    public function checkToken(): bool
    {
        return isset($this->token);
    }
}