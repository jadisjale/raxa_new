<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 13/01/19
 * Time: 22:07
 */

namespace App\Service\Login;

use Symfony\Component\Validator\Constraints as Assert;


class Login
{
    /**
     *
     * @Assert\Type(
     *     type="integer",
     *     message="O valor {{ value }} tem que ser do tipo {{ type }}."
     * )
     * @Assert\NotBlank(message="Não foi informado o grupo")
     *
     */
    private $id_group;

    /**
     *
     * @Assert\NotBlank(message="Não foi informado o login")
     *
     */
    private $login;

    /**
     *
     * @Assert\NotBlank(message="Não foi informado a senha")
     *
     */
    private $password;

    private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return Login
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdGroup()
    {
        return $this->id_group;
    }

    /**
     * @param mixed $id_group
     * @return Login
     */
    public function setIdGroup($id_group)
    {
        $this->id_group = $id_group;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return Login
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Login
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}