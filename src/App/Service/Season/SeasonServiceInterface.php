<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 15:12
 */

namespace App\Service\Season;


interface SeasonServiceInterface
{
    public function save(array $data);
    public function all(string $token);
}