<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 06/07/2019
 * Time: 15:17
 */

namespace App\Service\Season;

use App\Entity\Group;
use App\Entity\Season;
use App\Exception\ValidationException;
use App\Factory\Season\SeasonFactory;
use App\Repository\Season\SeasonRepositoryInterface;
use App\Service\Login\Session\SessionInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;

class SeasonService implements SeasonServiceInterface
{

    use ValidatorTrait;
    use ResponseTrait;
    /**
     * @var EntityManager
     */
    private $em;

    /** @var SeasonRepositoryInterface */
    private $repository;

    /** @var SessionInterface  */
    private $session;


    /**
     * SeasonService constructor.
     * @param EntityManager $em
     * @param SessionInterface $session
     */
    public function __construct(EntityManager $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Season::class);
        $this->session = $session;
    }

    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->session->setToken($data['token']);
            /** @var Group $group */
            $group = $this->session->getGroupUser()->getGroup();
            $season = SeasonFactory::make(array_merge($data, ['group' => $group]));
            $this->valid($season);
            /** @var Season $seasonOld */
            $seasonOld = $this->repository->getLastSeasonActive($group);
            if ($seasonOld) {
                $seasonOld->setEndDate(new \DateTime());
                $seasonOld->setIsActive(false);
                $this->em->persist($seasonOld);
                $this->em->flush();
            }
            $this->em->persist($season);
            $this->em->flush();
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Nova temporada salva');
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (\Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }

    }

    public function all(string $token)
    {
        try {
            $this->session->setToken($token);
            /** @var Group $group */
            $group = $this->session->getGroupToUser();
            $seasons = $this->repository->getAllByGroupId($group);
            $data = [];
            /** @var Season $season */
            foreach ($seasons as $season) {
                $data[] = [
                    'id' => $season->getId(),
                    'descrpition' => $season->getDescrpition(),
                    'isActive' => $season->getisActive(),
                ];
            }
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $data
            );
        } catch (\Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}