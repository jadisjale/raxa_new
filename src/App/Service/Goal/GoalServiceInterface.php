<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 15:52
 */

namespace App\Service\Goal;


use App\Entity\User;

interface GoalServiceInterface
{
    // CRIA UM NOVO GOAL
    public function save(array $data);

    // ATUALIZA O GOAL
    public function update($id, $array);

    // EXCLUI UM GOAL
    public function delete($id);

    // LISTA TODOS OS GOAL
    public function all();

    // LISTA TODOS AS ESTATÍSTICAS
    public function statistics($token);
    public function statisticsByGroupUser($token, $id_group_user);
    public function average($token);

    public function getListVictory($token);
    public function getListDraw();
    public function getListDefeat();
    public function getListGoals($token);
    public function getListAssistance();
    public function getListFrequency($token);

    public function saveNewGoal(int $user, array $data);

    public function getHistory($token);
    public function getHistoryById($id_group_user);

    public function getListVictoryBySession(int $idSeason);
    public function getListGoalsBySession(int $idSeason);
    public function getListFrequencyBySession(int $idSeason);

}