<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 10/01/2019
 * Time: 15:52
 */

namespace App\Service\Goal;


use App\Entity\Goal;
use App\Entity\GroupUser;
use App\Entity\Season;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Factory\Goal\GoalFactory;
use App\Repository\Goal\GoalRepository;
use App\Repository\GroupUser\GroupUserRepository;
use App\Repository\Season\SeasonRepositoryInterface;
use App\Service\Login\Session\SessionInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;
use Throwable;


class GoalService implements GoalServiceInterface
{

    use ValidatorTrait;
    use ResponseTrait;


    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var GoalRepository
     */
    private $goalRepository;

    /**
     * @var GroupUserRepository
     */
    private $groupUserRepository;

    /**
     * @var SeasonRepositoryInterface
     */
    private $seasonRepository;

    /**
     * @var Goal
     */
    private $goal;
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * GoalService constructor.
     * @param EntityManager $em
     * @param SessionInterface $session
     */
    public function __construct(EntityManager $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->goalRepository = $this->em->getRepository(Goal::class);
        $this->groupUserRepository = $this->em->getRepository(GroupUser::class);
        $this->seasonRepository = $this->em->getRepository(Season::class);
        $this->session = $session;
    }

    public function saveNewGoal(int $user_id, array $data)
    {
        try {
            $user = $this->em->getRepository(User::class)->find($user_id);
            if (!$user) {
               throw new \Exception('Usuário não encontrado', 9999);
            }
            $groupUser = $this->groupUserRepository->findOneBy([
                'user' => $user
            ]);
            if (!$groupUser) {
                throw new \Exception('Grupo para usuário não encontrado', 9999);
            }
            /** @var Season $season */
            $season = $this->seasonRepository->getLastSeasonActive($groupUser->getGroup());
            if (!$season) {
                throw new \Exception('Não há uma temporada cadastrada ainda, fale com o Admin!', 9999);
            }
            $data['groupUser'] = $groupUser;
            $data['season'] = $season;
            $goal = GoalFactory::make($data);
            $this->valid($goal);
            $this->goalRepository->save($goal);
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Gravado com sucesso');
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->session->setToken($data['token']);
            $id_group_user = $this->session->getGroupUser()->getId();
            $groupUser = $this->groupUserRepository->getReference($id_group_user);
            /** @var Season $season */
            $season = $this->seasonRepository->getLastSeasonActive($groupUser->getGroup());
            if (!$season) {
                throw new \Exception('Não há uma temporada cadastrada ainda, fale com o Admin!', 9999);
            }
            $data['groupUser'] = $groupUser;
            $data['season'] = $season;
            $goal = GoalFactory::make($data);
            $this->valid($goal);
            $this->em->persist($goal);
            $this->em->flush();
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $goal->getGroupUser()->getUser()->getPeople()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $e->getMessage(), $e->getMessage());
        }
    }


    public function update($id, $array)
    {
        try {
            $this->goal = $this->goalRepository->getReference($id);
            $this->goal
                ->setVictory((integer)$array['victory'])
                ->setDraw((integer)$array['draw'])
                ->setDefeat((integer)$array['defeat'])
                ->setGoals((integer)$array['goals'])
                ->setDate(new \DateTime($array['date']))
                ->setAssistance((integer)$array['assistance'])
            ;
            $this->valid($this->goal);
            $this->goalRepository->save($this->goal);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goal->getGroupUser()->getUser()->getPeople()->getName() . ', ATUALIZADO COM SUCESSO!');
        } catch (ValidationException $e) {
            $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function delete($data)
    {
        try {
            $this->session->setToken($data['token']);
            /** @var User $user */
            $user = $this->session->getGroupUser()->getUser();
            if ($user->getIsAdmin() === false) {
                throw new \Exception('Você não é um administrador');
            }
            $this->goalRepository->delete($data['id']);
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Raxa removico!');
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function statistics($token)
    {
        try {
            $this->session->setToken($token);
            $id_group_user = $this->session->getGroupUser()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->statistics($id_group_user));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function statisticsByGroupUser($token, $id_group_user)
    {
        try {
            $this->session->setToken($token);
            $you_id_group_user = $this->session->getGroupUser()->getId();
            $you = $this->goalRepository->statistics($you_id_group_user);
            $friend =  $this->goalRepository->statistics($id_group_user);
            $data = [
                'you' => $you,
                'friend' => $friend,
            ];
            return $this->success($this->codeSuccess, $this->messageSuccess, $data);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function average($token)
    {
        try {
            $this->session->setToken($token);
            $id_group_user = $this->session->getGroupUser()->getId();
            $statistic = $this->goalRepository->statistics($id_group_user);
            $frequency = $this->goalRepository->getListFrequencyByIdGroupUser($id_group_user);
            $data = [
              'photo' => $statistic[0]['photo'],
              'name' => $statistic[0]['name'],
              'goals' => round(($statistic[0]['goals'] / $frequency['frequency']), 2),
              'victory' => round(($statistic[0]['victory'] / $frequency['frequency']), 2),
              'frequency' => $frequency['frequency']
            ];
            return $this->success($this->codeSuccess, $this->messageSuccess, $data);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListVictory($token)
    {
        try {
            $this->session->setToken($token);
            $id_group = $this->session->getGroupUser()->getGroup()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getListVictory($id_group));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListDraw()
    {
        // TODO: Implement getListDraw() method.
    }

    public function getListDefeat()
    {
        // TODO: Implement getListDefeat() method.
    }

    public function getListGoals($token)
    {
        try {
            $this->session->setToken($token);
            $id_group = $this->session->getGroupUser()->getGroup()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getListGoals($id_group));
        } catch (\Exception $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListAssistance()
    {
        try {
//            $this->session->setToken($token);
            $id_group = $this->session->getGroupUser()->getGroup()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getListFrequency($id_group));
        } catch (\Exception $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListFrequency($token)
    {
        try {
            $this->session->setToken($token);
            $id_group = $this->session->getGroupUser()->getGroup()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getListFrequency($id_group));
        } catch (\Exception $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getHistory($token)
    {
        try {
            $this->session->setToken($token);
            $id_group_user = $this->session->getGroupUser()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getHistory($id_group_user));
        } catch (\Exception $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getHistoryById($id_group_user)
    {
        try {
//            $this->session->setToken($token);
//            $id_group_user = $this->session->getGroupUser()->getId();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->goalRepository->getHistory($id_group_user));
        } catch (\Exception $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListVictoryBySession(int $idSeason)
    {
        try {
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $this->goalRepository->getListVictoryBySeason($idSeason)
            );
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListGoalsBySession(int $idSeason)
    {
        try {
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $this->goalRepository->getListGoalsBySeason($idSeason)
            );
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getListFrequencyBySession(int $idSeason)
    {
        try {
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $this->goalRepository->getListFrequencyBySeason($idSeason)
            );
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}