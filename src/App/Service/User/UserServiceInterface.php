<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 13:59
 */

namespace App\Service\User;

interface UserServiceInterface
{
    public function save(array $data);
    public function photo(array $data);
    public function profile(array $data);
    public function saveUserAndNewGroup(array $data);
    public function all();
    public function getUsersByGroup($token);
    public function logout(int $id_user);

    /** Alterar login e senha do usuário
     * @param $token
     * @param array $data
     */
    public function updateDataUser($token, array $data);
    public function getById($token);
    public function tokenFcm(array $data);
}