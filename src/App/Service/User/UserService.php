<?php
/**
 * Created by PhpStorm.
 * User: jadisjale
 * Date: 05/01/19
 * Time: 14:00
 */

namespace App\Service\User;


use App\Entity\Group;
use App\Entity\People;
use App\Entity\Season;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Factory\Group\GroupFactory;
use App\Factory\Season\SeasonFactory;
use App\Factory\User\UserFactory;
use App\Repository\Group\GroupRepository;
use App\Repository\Season\SeasonRepositoryInterface;
use App\Service\GroupUser\GroupUserServiceInterface;
use App\Service\Login\Session\Session;
use App\Service\Login\Session\SessionInterface;
use App\Service\People\PeopleServiceInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;
use Throwable;


class UserService implements UserServiceInterface
{

    use ValidatorTrait;
    use ResponseTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \App\Repository\User\UserRepository $userRepository
     */
    private $userRepository;

    /**
     * @var PeopleServiceInterface
     */
    private $peopleService;

    /**
     * @var GroupUserServiceInterface
     */
    private $groupUserService;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /** @var SessionInterface  */
    private $session;

    /** @var SeasonRepositoryInterface */
    private $seasonRepository;

    /**
     * UserService constructor.
     * @param EntityManager $em
     * @param PeopleServiceInterface $peopleService
     * @param GroupUserServiceInterface $groupUserService
     * @param SessionInterface $session
     */
    public function __construct(
        EntityManager $em,
        PeopleServiceInterface $peopleService,
        GroupUserServiceInterface $groupUserService,
        SessionInterface $session
    )
    {
        $this->session = $session;
        $this->em = $em;
        $this->peopleService = $peopleService;
        $this->groupUserService = $groupUserService;
        $this->userRepository = $this->em->getRepository(User::class);
        $this->groupRepository = $this->em->getRepository(Group::class);
        $this->seasonRepository = $this->em->getRepository(Season::class);
    }

    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->session->setToken($data['token']);
            $people = $this->peopleService->save($data);
            $user = UserFactory::make(array_merge($data, ['people' => $people]));
            $this->valid($data);
            $group = $this->session->getGroupUser()->getGroup();
            $this->userRepository->save($user);
            $this->groupUserService->save(array_merge(['user' => $user, 'group' => $group]));
            $this->em->commit();

            return $this->success($this->codeSuccess, $this->messageSuccess, $user->getPeople()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function photo(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->session->setToken($data['token']);
            $photo = $data['photo'];
            $id_user = $this->session->getUserLogged()->getId();
//            var_dump($id_user);
//            var_dump($photo);
//            die;
            $this->userRepository->photo($id_user, $photo);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Foto adicionada com sucesso');
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function all()
    {
        try{
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->userRepository->all());
        }catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getUsersByGroup($token)
    {
        try{
            $this->session->setToken($token);
            $id_group = $this->session->getGroupUser()->getGroup()->getId();
            $id_user = $this->session->getGroupUser()->getUser()->getId();
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $this->userRepository->getUsersByGroup($id_group, $id_user)
            );
        }catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function logout(int $id_user)
    {
        try {
        /** @var User $user */
        $user = $this->userRepository->find($id_user);
        if (!$user) {
            throw new \Exception('Usuário não encontrado', 9999);
        }

        return $this->success(
            $this->codeSuccess,
            $this->messageSuccess,
            $this->userRepository->logout($id_user)
        );

        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, 'Não foi possível deslogar');
        }
    }

    public function saveUserAndNewGroup(array $data)
    {
        try {
            $this->em->beginTransaction();

            $group = GroupFactory::makeWithNewUser($data);
            $this->valid($group);
            $this->groupRepository->save($group);

            $people = $this->peopleService->save($data);
            $user = UserFactory::make(array_merge($data, ['people' => $people]));
            $this->valid($data);
            $this->userRepository->save($user);
            $this->groupUserService->save(array_merge(['user' => $user, 'group' => $group]));

            $season = SeasonFactory::make(array_merge($data, ['group' => $group]));
            $this->valid($season);
            $this->seasonRepository->save($season);

            $this->em->commit();

            return $this->success($this->codeSuccess, $this->messageSuccess, $group->getId());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function profile(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->session->setToken($data['token']);
            /** @var User $user */
            $user = $this->userRepository->find($this->session->getGroupUser()->getUser()->getId());
            if (!$user) {
                return $this->warning($this->codeWarning, $this->messageWarning, 'Não foi encontrado o usuário');
            }
            $user->setPassword($data['password']);
            $this->userRepository->save($user);

            if ($data['name']) {
                /** @var People $people */
                $people = $this->em->getRepository(People::class)->findOneBy([
                    'id' => $user->getPeople()->getId()
                ]);
                $people->setName($data['name']);
                $this->em->persist($people);
                $this->em->flush();
            }

            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $user->getPeople()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param $token
     * @param array $data
     * @return string
     */
    public function updateDataUser($token, array $data)
    {
        try{
            $this->em->beginTransaction();
            $this->session->setToken($token);
            $id_user = $this->session->getGroupUser()->getUser()->getId();
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($id_user);
            $user->setPassword(md5($data['password']));
            $user->setLogin($data['login']);

            $people = $this->em->getRepository(People::class)->find($user->getPeople()->getId());
            $people->setName($data['name']);

            $this->em->persist($user);
            $this->em->persist($people);

            $this->em->flush();
            $this->valid($user);
            $this->valid($people);

            $this->em->commit();
            return $this->success(
                $this->codeSuccess,
                $this->messageSuccess,
                $user
            );
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    public function getById($token) {
        $this->em->beginTransaction();
        $this->session->setToken($token);
        $id_user = $this->session->getGroupUser()->getUser()->getId();

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($id_user);

        $data['name'] = $user->getPeople()->getName();
        $data['login'] = $user->getLogin();

        return $this->success(
            $this->codeSuccess,
            $this->messageSuccess,
            $data
        );
    }

    public function tokenFcm(array $data)
    {
        $this->session->setToken($data['token']);
        $id_user = $this->session->getGroupUser()->getUser()->getId();
        /** @var User */
        $user = $this->em->getRepository(User::class)->find($id_user);
        $user->setTokenFcm($data['token_fcm']);
        $this->userRepository->save($user);
        return $this->success(
            $this->codeSuccess,
            $this->messageSuccess,
            $data
        );
    }
}