<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 26/11/2019
 * Time: 22:07
 */

namespace App\Service\Improvement;


interface ImprovementServiceInterface
{
    public function save(array $data);

    public function all();
}