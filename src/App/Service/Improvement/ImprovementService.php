<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 26/11/2019
 * Time: 22:08
 */

namespace App\Service\Improvement;


use App\Entity\Improvement;
use App\Exception\ValidationException;
use App\Factory\Improvement\ImprovementFactory;
use App\Service\Login\Session\SessionInterface;
use App\Traits\ResponseTrait;
use App\Traits\ValidatorTrait;
use Doctrine\ORM\EntityManager;
use Exception;

class ImprovementService implements ImprovementServiceInterface
{

    use ValidatorTrait;
    use ResponseTrait;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var \App\Repository\Improvement\ImprovementRepository
     */
    private $repository;

    /** @var SessionInterface  */
    private $session;

    /**
     * ImprovementService constructor.
     * @param EntityManager $em
     * @param SessionInterface $session
     */
    public function __construct(EntityManager $em, SessionInterface $session)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Improvement::class);
        $this->session = $session;
    }


    /**
     * @param array $data
     * @return string
     * @throws \App\Exception\ValidationException
     * @throws \Exception
     */
    public function save(array $data)
    {
        try {
            $this->session->setToken($data['token']);
            $user = $this->session->getGroupUser()->getUser();
            $data['user'] = $user;
            $improvement = ImprovementFactory::make($data);
            $this->valid($improvement);
            $this->repository->save($improvement);
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Sugestão anotada');
        } catch (ValidationException $e) {
            return $this->success($this->codeSuccess, $this->messageError, $e->getMessage());
        } catch (Exception $e) {
            return $this->success($this->codeSuccess, $this->messageError, $e->getMessage());
        }
    }

    public function all()
    {
        return $this->success(
            $this->codeSuccess,
            $this->messageError,
            $this->repository->findAll()
        );
    }
}