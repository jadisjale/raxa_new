<?php

if (session_id() == '' || !isset($_SESSION)) {
    session_start();
}

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__ . '/../../vendor/autoload.php';

use App\Middleware\AuthMiddleware;

try {
    $di = require __DIR__ . '/../bootstrap/container.php';

    $configuration = [
        'settings' => [
            'displayErrorDetails' => true,
        ]
    ];

    $chave = 'OTg5MzA3ODE5';
    $chave2 = 'OTg5NDAyNDIx';
    $chave5 = 'ODUgODk2MDkyMTM=';

    $container = new Slim\Container($configuration);
    $container['di'] = $di;

    $app = new \Slim\App($container);

//    $app->add(new \Slim\Middleware\Session([
//        'teste' => '112233'
//    ]));

    $app->group('/improviment', function () {
        $this->post('', '\App\Controller\ImprovementController:save');
        $this->get('', '\App\Controller\GroupController:all');
    });

    $app->group('/group', function () {
        $this->post('', '\App\Controller\GroupController:save');
        $this->get('', '\App\Controller\GroupController:all');
    });

    $app->group('/user', function () {
        $this->post('', '\App\Controller\UserController:save');
//        $this->get('/all', '\App\Controller\UserController:all');
        $this->get('/all/{token}', '\App\Controller\UserController:getUsersByGroup');
        $this->post('/photo/{token}', '\App\Controller\UserController:photo');
        $this->post('/new/group', '\App\Controller\UserController:saveUserAndNewGroup');
        $this->post('/profile', '\App\Controller\UserController:profile');
        $this->post('/update/data/{token}', '\App\Controller\UserController:updateDataUser');
        $this->post('/{token}', '\App\Controller\UserController:getById');
        $this->post('/token/fcm', '\App\Controller\UserController:tokenFcm');
    });

    $app->group('/goal', function () {
        $this->post('/save', '\App\Controller\GoalController:save');
        $this->post('/save/{user}', '\App\Controller\GoalController:saveNewGoal');
        $this->put('/update/{id}', '\App\Controller\GoalController:update');
        $this->post('/delete/{token}', '\App\Controller\GoalController:delete');
        $this->get('/all', '\App\Controller\GoalController:all');
        $this->get('/statistics/{token}', '\App\Controller\GoalController:statistics');
        $this->get('/statistics/{token}/{id}', '\App\Controller\GoalController:statisticsToFriend');
        $this->get('/average/{token}', '\App\Controller\GoalController:average');
        $this->get('/frequency', '\App\Controller\GoalController:getListFrequency');
        $this->get('/goals/{token}', '\App\Controller\GoalController:getListGoals');
        $this->get('/victorious/{token}', '\App\Controller\GoalController:getListVictory');
        $this->get('/frequency/{token}', '\App\Controller\GoalController:getListFrequency');
        $this->get('/history/{token}', '\App\Controller\GoalController:getHistory');
        $this->get('/history/group/user/{id}', '\App\Controller\GoalController:getHistoryById');

        $this->get('/goals/season/{id}', '\App\Controller\GoalController:getListGoalsBySession');
        $this->get('/victorious/season/{id}', '\App\Controller\GoalController:getListVictoryBySession');
        $this->get('/frequency/season/{id}', '\App\Controller\GoalController:getListFrequencyBySession');

    })->add(new AuthMiddleware());

    $app->group('/login', function () {
        $this->post('', '\App\Controller\LoginController:auth');
        $this->get('/logout/{token}', '\App\Controller\LoginController:logout');
    });

    $app->group('/group/user', function () {
        $this->get('/all', '\App\Controller\GroupUserController:all');
    });

    $app->group('/season', function () {
        $this->post('/save', '\App\Controller\SeasonController:save');
        $this->get('/all/{token}', '\App\Controller\SeasonController:getAllSeasonByGroup');
    });

    $app->run();

} catch (Throwable $e) {
    $e->getMessage();
}